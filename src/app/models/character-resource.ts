export class CharacterResource {
  id: number;
  name: string;
  icon: string;
  currentQuantity: number;
  maximumQuantity: number;
  recoveryType: string;
  displayType: number;
}
