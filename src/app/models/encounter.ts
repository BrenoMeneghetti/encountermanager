import { Character } from './character';

export class Encounter {
  name: string;
  currentTurn: number;
  characters: Character[];
}
