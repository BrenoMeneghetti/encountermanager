import { CharacterResource } from './character-resource';

export class PlayerResource extends CharacterResource {
  playerId: number;
  constructor() {
    super();
  }
}
