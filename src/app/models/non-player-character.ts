import { Character } from './character';

export class NonPlayerCharacter extends Character {
  challengeRating: number;
  averageHitPoints: number;
  constructor() {
    super();
  }
}
