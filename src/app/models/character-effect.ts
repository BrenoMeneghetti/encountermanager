export class CharacterEffect {
  id: number;
  name: string;
  characterSourceId: number;
  turnsDuration: number;
  turnsRemaining: number;
  turnCountType: string;
  turnBindType: string;
  savePerTurn: string;
  concentration: boolean;
  affectedCharactersId: number[];
}
