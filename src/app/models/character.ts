import { CharacterResource } from './character-resource';

export class Character {
  id: number;
  name: string;
  currentHitPoints: number;
  maximumHitPoints: number;
  initiativeBonus: number;
  resources: CharacterResource[];
  constructor() {

  }
}
