import { playersReducer, playerListInitialState } from './players.reducer';
import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { PlayerCharacter } from '../../models/player-character';
import { countersReducer, Counters, countersInitialState } from './counters.reducer';
import { PlayerResource } from '../../models/player-resource';
import { playerResourceInitialState, playerResourceReducer } from './player-resource.reducer';
import { playerResourceEditReducer } from './player-resource-edit.reducer';
import { playerEditInitialState, playerEditReducer } from './player-edit.reducer';
import { characterEffectsReducer, characterEffectsInitialState } from './character-effects.reducer';
import { CharacterEffect } from '../../models/character-effect';
import { npcListInitialState } from './npcs.reducer';
import { NonPlayerCharacter } from '../../models/non-player-character';

export const reducers = {
  players: playersReducer,
  npcs: playersReducer,
  resources: playerResourceReducer,
  counters: countersReducer,
  playerResourceEdit: playerResourceEditReducer,
  playerEdit: playerEditReducer,
  characterEffects: characterEffectsReducer
};

export interface IAppState {
  players: PlayerCharacter[];
  npcs: NonPlayerCharacter[];
  resources: PlayerResource[];
  counters: Counters;
  currentTurn: PlayerCharacter;
  playerResourceEdit: PlayerResource;
  playerEdit: PlayerCharacter;
  characterEffects: CharacterEffect[];
}

export const INITIAL_STATE: IAppState = {
  players: playerListInitialState,
  npcs: npcListInitialState,
  resources: playerResourceInitialState,
  counters: countersInitialState,
  currentTurn: null,
  playerResourceEdit: null,
  playerEdit: playerEditInitialState,
  characterEffects: characterEffectsInitialState
};

export const reducerToken = new InjectionToken<ActionReducerMap<IAppState>>(
  'Reducers'
);

export function getReducers() {
  return reducers;
}

export const reducerProvider = [
  { provide: reducerToken, useFactory: getReducers }
];


