import * as PlayerActions from '../actions/player.actions';
import { PlayerCharacter } from '../../models/player-character';

export const playerListInitialState = [];

// Section 2
export function playersReducer(
  state: PlayerCharacter[] = playerListInitialState,
  action: PlayerActions.Actions
) {
  // Section 3
  switch (action.type) {
    case PlayerActions.LOAD_PLAYERLIST:
      return action.payload;
    case PlayerActions.ADD_PLAYER:
      return [...state, action.payload];

    case PlayerActions.REMOVE_PLAYER:
      return state.filter(item => item.id !== action.payload.id);

    case PlayerActions.UPDATE_PLAYER:
      return state.map(
        item =>
          item.id === action.payload.id
            ? Object.assign({}, item, action.payload)
            : item
      );
    default:
      return state;
  }
}
