import * as CountersActions from '../actions/counters.actions';

export interface Counters {
  characterCounter: number;
  resourceCounter: number;
  effectCounter: number;
}

export const countersInitialState = {
  characterCounter: 1,
  resourceCounter: 1,
  effectCounter: 1
};

// Section 2
export function countersReducer(
  state: Counters = countersInitialState,
  action: CountersActions.Actions
) {
  // Section 3
  switch (action.type) {
    case CountersActions.INCREMENT_CHARACTER_COUNTER:
      return Object.assign({}, state, {
        playerCounter: state.characterCounter + 1
      });
    case CountersActions.INCREMENT_RESOURCE_COUNTER:
      return Object.assign({}, state, {
        resourceCounter: state.resourceCounter + 1
      });
    case CountersActions.INCREMENT_EFFECT_COUNTER:
      return Object.assign({}, state, {
        effectCounter: state.effectCounter + 1
      });
    case CountersActions.LOAD_CHARACTER_COUNTER:
      return Object.assign({}, state, {
        playerCounter: action.payload
      });
    case CountersActions.LOAD_RESOURCE_COUNTER:
      return Object.assign({}, state, {
        resourceCounter: action.payload
      });
    case CountersActions.LOAD_EFFECT_COUNTER:
      return Object.assign({}, state, {
        effectCounter: action.payload
      });

    default:
      return state;
  }
}
