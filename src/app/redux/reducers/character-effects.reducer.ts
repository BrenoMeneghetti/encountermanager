import * as CharacterEffectActions from '../actions/character-effects.actions';
import { CharacterEffect } from '../../models/character-effect';

export const characterEffectsInitialState = [];

// Section 2
export function characterEffectsReducer(
  state: CharacterEffect[] = characterEffectsInitialState,
  action: CharacterEffectActions.Actions
) {
  // Section 3
  switch (action.type) {
    case CharacterEffectActions.LOAD_CHARACTER_EFFECT_LIST:
      return action.payload;
    case CharacterEffectActions.ADD_CHARACTER_EFFECT:
      return [...state, action.payload];

    case CharacterEffectActions.REMOVE_CHARACTER_EFFECT:
      return state.filter(item => item.id !== action.payload.id);

    case CharacterEffectActions.UPDATE_CHARACTER_EFFECT:
      return state.map(
        item =>
          item.id === action.payload.id
            ? Object.assign({}, item, action.payload)
            : item
      );
    default:
      return state;
  }
}
