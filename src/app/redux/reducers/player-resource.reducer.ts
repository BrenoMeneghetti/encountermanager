import * as PlayerResourceActions from '../actions/player-resources.actions';
import { PlayerResource } from '../../models/player-resource';

export const playerResourceInitialState = [];

// Section 2
export function playerResourceReducer(
  state: PlayerResource[] = playerResourceInitialState,
  action: PlayerResourceActions.Actions
) {
  // Section 3
  switch (action.type) {
    case PlayerResourceActions.ADD_PLAYER_RESOURCE:
      return [...state, action.payload];

    case PlayerResourceActions.REMOVE_PLAYER_RESOURCE:
      return state.filter(resource => resource.id !== action.payload);

    case PlayerResourceActions.UPDATE_PLAYER_RESOURCE:
      return state.map(
        resource =>
          resource.id === action.payload.id
            ? Object.assign({}, resource, action.payload)
            : resource
      );

    case PlayerResourceActions.USE_PLAYER_RESOURCE:
      return state.map(
        resource =>
          resource.id === action.payload
            ? Object.assign({}, resource, { currentQuantity: resource.currentQuantity - 1 })
            : resource
      );

    case PlayerResourceActions.RECOVER_PLAYER_RESOURCE:
      return state.map(
        resource =>
          resource.id === action.payload
            ? Object.assign({}, resource, { currentQuantity: resource.currentQuantity + 1 })
            : resource
      );

    case PlayerResourceActions.REPLACE_PLAYER_RESOURCES:
      return action.payload;
    default:
      return state;
  }
}
