import * as NPCActions from '../actions/npc.actions';
import { NonPlayerCharacter } from '../../models/non-player-character';

export const npcListInitialState = [];

// Section 2
export function npcsReducer(
  state: NonPlayerCharacter[] = npcListInitialState,
  action: NPCActions.Actions
) {
  // Section 3
  switch (action.type) {
    case NPCActions.LOAD_NPC_LIST:
      return action.payload;
    case NPCActions.ADD_NPC:
      return [...state, action.payload];

    case NPCActions.REMOVE_NPC:
      return state.filter(item => item.id !== action.payload.id);

    case NPCActions.UPDATE_NPC:
      return state.map(
        item =>
          item.id === action.payload.id
            ? Object.assign({}, item, action.payload)
            : item
      );
    default:
      return state;
  }
}
