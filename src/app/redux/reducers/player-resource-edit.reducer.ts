import * as PlayerResourceEditActions from '../actions/player-resource-edit.actions';
import { PlayerResource } from '../../models/player-resource';

export const playerResourceEditInitialState = null;

// Section 2
export function playerResourceEditReducer(
  state: PlayerResource = playerResourceEditInitialState,
  action: PlayerResourceEditActions.Actions
) {
  // Section 3
  switch (action.type) {
    case PlayerResourceEditActions.START_RESOURCE_PLAYER_EDIT:
      return action.payload;

    case PlayerResourceEditActions.END_RESOURCE_PLAYER_EDIT:
      return null;

    default:
      return state;
  }
}
