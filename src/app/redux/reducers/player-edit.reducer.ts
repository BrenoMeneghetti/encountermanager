import * as PlayerEditActions from '../actions/player-edit.actions';
import { PlayerCharacter } from '../../models/player-character';

export const playerEditInitialState = null;

// Section 2
export function playerEditReducer(
  state: PlayerCharacter = playerEditInitialState,
  action: PlayerEditActions.Actions
) {
  // Section 3
  switch (action.type) {
    case PlayerEditActions.START_PLAYER_EDIT:
      return Object.assign({}, action.payload);

    case PlayerEditActions.END_PLAYER_EDIT:
      return null;

    default:
      return state;
  }
}
