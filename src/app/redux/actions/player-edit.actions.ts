import { Action } from '@ngrx/store';
import { PlayerCharacter } from '../../models/player-character';

// Section 2
export const START_PLAYER_EDIT = '[Player] Start Edit';
export const END_PLAYER_EDIT = '[Player] End Edit';

// Section 3
export class StartPlayerEdit implements Action {
  readonly type = START_PLAYER_EDIT;
  constructor(public payload: PlayerCharacter) {}
}

export class EndPlayerEdit implements Action {
  readonly type = END_PLAYER_EDIT;
  constructor() {}
}


// Section 4
export type Actions =
  | StartPlayerEdit
  | EndPlayerEdit;
