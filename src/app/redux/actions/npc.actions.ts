// Section 1
import { Action } from '@ngrx/store';
import { NonPlayerCharacter } from '../../models/non-player-character';
// Section 2
export const ADD_NPC = '[NPC] Add';
export const REMOVE_NPC = '[NPC] Remove';
export const UPDATE_NPC = '[NPC] Update';
export const LOAD_NPC_LIST = '[NPC] List';

// Section 3
export class AddNPC implements Action {
  readonly type = ADD_NPC;
  constructor(public payload: NonPlayerCharacter) {}
}

export class RemoveNPC implements Action {
  readonly type = REMOVE_NPC;
  constructor(public payload: NonPlayerCharacter) {}
}

export class UpdateNPC implements Action {
  readonly type = UPDATE_NPC;
  constructor(public payload: NonPlayerCharacter) {}
}

export class LoadNPCList implements Action {
  readonly type = LOAD_NPC_LIST;
  constructor(public payload: NonPlayerCharacter[]) {}
}

// Section 4
export type Actions =
  | AddNPC
  | RemoveNPC
  | UpdateNPC
  | LoadNPCList;
