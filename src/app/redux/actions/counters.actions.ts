// Section 1
import { Action } from '@ngrx/store';

// Section 2
export const INCREMENT_CHARACTER_COUNTER = '[CharacterCounter] Increment';
export const LOAD_CHARACTER_COUNTER = '[CharacterCounter] Load';
export const INCREMENT_RESOURCE_COUNTER = '[ResourceCounter] Increment';
export const LOAD_RESOURCE_COUNTER = '[ResourceCounter] Load';
export const INCREMENT_EFFECT_COUNTER = '[EffectCounter] Increment';
export const LOAD_EFFECT_COUNTER = '[EffectCounter] Load';

// Section 3
export class IncrementCharacterCounter implements Action {
  readonly type = INCREMENT_CHARACTER_COUNTER;
  constructor() {}
}

export class LoadCharacterCounter implements Action {
  readonly type = LOAD_CHARACTER_COUNTER;
  constructor(public payload: number) {}
}

export class IncrementResourceCounter implements Action {
  readonly type = INCREMENT_RESOURCE_COUNTER;
  constructor( ) {}
}

export class LoadResourceCounter implements Action {
  readonly type = LOAD_RESOURCE_COUNTER;
  constructor(public payload: number) {}
}

export class IncrementEffectCounter implements Action {
  readonly type = INCREMENT_EFFECT_COUNTER;
  constructor( ) {}
}

export class LoadEffectCounter implements Action {
  readonly type = LOAD_EFFECT_COUNTER;
  constructor(public payload: number) {}
}


// Section 4
export type Actions =
  | IncrementCharacterCounter
  | IncrementResourceCounter
  | IncrementEffectCounter
  | LoadCharacterCounter
  | LoadEffectCounter
  | LoadResourceCounter;
