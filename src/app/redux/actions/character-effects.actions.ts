// Section 1
import { Action } from '@ngrx/store';
import { CharacterEffect } from '../../models/character-effect';

// Section 2
export const ADD_CHARACTER_EFFECT = '[CharacterEffect] Add';
export const REMOVE_CHARACTER_EFFECT = '[CharacterEffect] Remove';
export const UPDATE_CHARACTER_EFFECT = '[CharacterEffect] Update';
export const LOAD_CHARACTER_EFFECT_LIST = '[CharacterEffect] List';

// Section 3
export class AddCharacterEffect implements Action {
  readonly type = ADD_CHARACTER_EFFECT;
  constructor(public payload: CharacterEffect) {}
}

export class RemoveCharacterEffect implements Action {
  readonly type = REMOVE_CHARACTER_EFFECT;
  constructor(public payload: CharacterEffect) {}
}

export class UpdateCharacterEffect implements Action {
  readonly type = UPDATE_CHARACTER_EFFECT;
  constructor(public payload: CharacterEffect) {}
}

export class LoadEffectList implements Action {
  readonly type = LOAD_CHARACTER_EFFECT_LIST;
  constructor(public payload: CharacterEffect[]) {}
}

// Section 4
export type Actions =
  | AddCharacterEffect
  | RemoveCharacterEffect
  | UpdateCharacterEffect
  | LoadEffectList;
