import { Action } from '@ngrx/store';
import { PlayerResource } from '../../models/player-resource';

// Section 2
export const ADD_PLAYER_RESOURCE = '[PlayerResource] Add';
export const REMOVE_PLAYER_RESOURCE = '[PlayerResource] Remove';
export const UPDATE_PLAYER_RESOURCE = '[PlayerResource] Update';
export const USE_PLAYER_RESOURCE = '[PlayerResource] Use';
export const RECOVER_PLAYER_RESOURCE = '[PlayerResource] Recover';
export const REPLACE_PLAYER_RESOURCES = '[PlayerResource] Replace';

// Section 3
export class AddPlayerResource implements Action {
  readonly type = ADD_PLAYER_RESOURCE;
  constructor(public payload: PlayerResource) {}
}

export class RemovePlayerResource implements Action {
  readonly type = REMOVE_PLAYER_RESOURCE;
  constructor(public payload: number) {}
}

export class UpdatePlayerResource implements Action {
  readonly type = UPDATE_PLAYER_RESOURCE;
  constructor(public payload: PlayerResource) {}
}

export class UsePlayerResource implements Action {
  readonly type = USE_PLAYER_RESOURCE;
  constructor(public payload: number) {}
}

export class RecoverPlayerResource implements Action {
  readonly type = RECOVER_PLAYER_RESOURCE;
  constructor(public payload: number) {}
}

export class ReplacePlayerResource implements Action {
  readonly type = REPLACE_PLAYER_RESOURCES;
  constructor(public payload: PlayerResource[]) {}
}

// Section 4
export type Actions =
  | AddPlayerResource
  | RemovePlayerResource
  | UpdatePlayerResource
  | UsePlayerResource
  | RecoverPlayerResource
  | ReplacePlayerResource;
