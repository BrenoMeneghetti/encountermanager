// Section 1
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { PlayerCharacter } from '../../models/player-character';
import { CharacterResource } from '../../models/character-resource';

// Section 2
export const ADD_PLAYER = '[Player] Add';
export const REMOVE_PLAYER = '[Player] Remove';
export const UPDATE_PLAYER = '[Player] Update';
export const LOAD_PLAYERLIST = '[Player] List';

// Section 3
export class AddPlayer implements Action {
  readonly type = ADD_PLAYER;
  constructor(public payload: PlayerCharacter) {}
}

export class RemovePlayer implements Action {
  readonly type = REMOVE_PLAYER;
  constructor(public payload: PlayerCharacter) {}
}

export class UpdatePlayer implements Action {
  readonly type = UPDATE_PLAYER;
  constructor(public payload: PlayerCharacter) {}
}

export class LoadList implements Action {
  readonly type = LOAD_PLAYERLIST;
  constructor(public payload: PlayerCharacter[]) {}
}

// Section 4
export type Actions =
  | AddPlayer
  | RemovePlayer
  | UpdatePlayer
  | LoadList;
