import { Action } from '@ngrx/store';
import { PlayerResource } from '../../models/player-resource';

// Section 2
export const START_RESOURCE_PLAYER_EDIT = '[PlayerResource] Start Edit';
export const END_RESOURCE_PLAYER_EDIT = '[PlayerResource] End Edit';

// Section 3
export class StartPlayerResourceEdit implements Action {
  readonly type = START_RESOURCE_PLAYER_EDIT;
  constructor(public payload: PlayerResource) {}
}

export class EndPlayerResourceEdit implements Action {
  readonly type = END_RESOURCE_PLAYER_EDIT;
  constructor() {}
}


// Section 4
export type Actions =
  | StartPlayerResourceEdit
  | EndPlayerResourceEdit;
