import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HitPointsControlComponent } from './components/hit-points-control/hit-points-control.component';
import { PlayerManagementComponent } from './pages/player-management/player-management.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { SummaryComponent } from './pages/summary/summary.component';
import { NpcManagementComponent } from './pages/npc-management/npc-management.component';
import { EncountersComponent } from './pages/encounters/encounters.component';
import { PlayerListComponent } from './components/player-list/player-list.component';
import { PlayerInformationComponent } from './components/player-information/player-information.component';
import { InitiativeControlComponent } from './components/initiative-control/initiative-control.component';
import { ReactionControlComponent } from './components/reaction-control/reaction-control.component';
import { SpellControllerComponent } from './components/spell-controller/spell-controller.component';
import { CharacterActionsComponent } from './components/character-actions/character-actions.component';
import { IconSelectorComponent } from './components/icon-selector/icon-selector.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducerToken, INITIAL_STATE, reducerProvider } from './redux/reducers';
import { StateStorageService } from './services/state-storage.service';
import { PlayerResourceComponent } from './components/player-resource/player-resource.component';
import { AlertifyService } from './services/alertify.service';
import { SideModalComponent } from './components/side-modal/side-modal.component';
import { PlayerResourceEditComponent } from './components/player-resource-edit/player-resource-edit.component';
import { PlayerEditComponent } from './components/player-edit/player-edit.component';
import { PlayerResourceListComponent } from './components/player-resource-list/player-resource-list.component';
import { CharacterEffectListComponent } from './components/character-effect-list/character-effect-list.component';
import { CharacterEffectEditComponent } from './components/character-effect-edit/character-effect-edit.component';
import { NPCListComponent } from './components/npc-list/npc-list.component';
import { NPCEditComponent } from './components/npc-edit/npc-edit.component';
import { NPCInformationComponent } from './components/npc-information/npc-information.component';
import { NPCResourceInformationComponent } from './components/npc-resource-information/npc-resource-information.component';
import { NPCResourceEditComponent } from './components/npc-resource-edit/npc-resource-edit.component';

const appRoutes: Routes = [
  { path: 'player-management', component: PlayerManagementComponent },
  { path: 'summary', component: SummaryComponent },
  { path: 'npc-management', component: NpcManagementComponent },
  { path: 'encounters', component: EncountersComponent },
  {
    path: '',
    redirectTo: '/summary',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HitPointsControlComponent,
    PlayerManagementComponent,
    NavBarComponent,
    SummaryComponent,
    NpcManagementComponent,
    EncountersComponent,
    PlayerListComponent,
    PlayerInformationComponent,
    InitiativeControlComponent,
    ReactionControlComponent,
    SpellControllerComponent,
    CharacterActionsComponent,
    IconSelectorComponent,
    PlayerResourceComponent,
    SideModalComponent,
    PlayerResourceEditComponent,
    PlayerEditComponent,
    PlayerResourceListComponent,
    CharacterEffectListComponent,
    CharacterEffectEditComponent,
    NPCListComponent,
    NPCEditComponent,
    NPCInformationComponent,
    NPCResourceInformationComponent,
    NPCResourceEditComponent,
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(reducerToken, { initialState: INITIAL_STATE }),
    StoreDevtoolsModule.instrument(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [reducerProvider, StateStorageService, AlertifyService],
  bootstrap: [AppComponent]
})
export class AppModule {}
