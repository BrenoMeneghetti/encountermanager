import { Injectable } from '@angular/core';
import { IAppState } from '../redux/reducers';

@Injectable({
  providedIn: 'root'
})
export class StateStorageService {
  private storageKey = 'ec.playerlist';
  constructor() {}

  loadState(): IAppState {
    const listJSON = localStorage.getItem(this.storageKey);
    const playerList: IAppState = JSON.parse(listJSON);
    return playerList;
  }

  saveState(state: IAppState) {
    const listJSON = JSON.stringify(state);
    localStorage.setItem(this.storageKey, listJSON);
  }
}
