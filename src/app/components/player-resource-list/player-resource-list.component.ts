import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../redux/reducers';
import { AlertifyService } from '../../services/alertify.service';
import { Observable } from 'rxjs';
import { PlayerResource } from '../../models/player-resource';
import { RemovePlayerResource, UpdatePlayerResource, UsePlayerResource, RecoverPlayerResource } from '../../redux/actions/player-resources.actions';
import { StartPlayerResourceEdit } from '../../redux/actions/player-resource-edit.actions';

@Component({
  selector: 'app-player-resource-list',
  template: `
    <div class="resource-list">
      <div class="add-player-resource">
        <button (click)="addResource(playerId)">Add <br> Resource</button>
      </div>
      <app-player-resource *ngFor="let resource of playerResources$ | async" [characterResource]="resource"
      (edit)="editResource($event)"
      (remove)="removeResource($event)"
      (display)="changeDisplay($event)"
      (use)="useResource($event)"
      (recover)="recoverResource($event)"
      ></app-player-resource>
    </div>
  `,
  styles: [`
  .resource-list{
    display: flex;
    padding: 0;
  }
  .resource-list > .column{
    padding-left: 5px;
    padding-right: 5px;
  }
  .add-player-resource{
    width: 100px;
    margin-right: 5px;
  }
  `]
})
export class PlayerResourceListComponent implements OnInit {
  @Input() playerId: number;
  playerResources$: Observable<PlayerResource[]>;
  constructor(private store: Store<IAppState>, private alertifyService: AlertifyService) {
    this.playerResources$ = store
      .select(state => state.resources ? state.resources.filter(resource => resource.playerId === this.playerId) : state.resources);
  }

  ngOnInit() {
  }

  recoverResource(playerResource: PlayerResource) {
    this.alertifyService.success(
      playerResource.name + ' has been recovered.'
    );
    this.store.dispatch(new RecoverPlayerResource(playerResource.id));
  }

  useResource(playerResource: PlayerResource) {
    this.alertifyService.success(
      playerResource.name + ' has been used.'
    );
    this.store.dispatch(new UsePlayerResource(playerResource.id));
  }

  addResource(playerId: number) {
    const newPlayer = new PlayerResource();
    newPlayer.playerId = playerId;
    this.store.dispatch(new StartPlayerResourceEdit(newPlayer));
  }

  editResource(playerResource: PlayerResource) {
    this.store.dispatch(new StartPlayerResourceEdit(playerResource));
  }

  removeResource(playerResource: PlayerResource) {
    this.alertifyService.confirm(
      'Remove Resource',
      'Do you really want to remove the resource ' +
      playerResource.name +
      '?',
      () => {
        this.store.dispatch(new RemovePlayerResource(playerResource.id));
        this.alertifyService.success(
          playerResource.name + ' has been removed.'
        );
      }
    );
  }

  changeDisplay(playerResource: PlayerResource) {
    playerResource.displayType = playerResource.displayType === 5 ? 1 : ++playerResource.displayType;
    this.store.dispatch(new UpdatePlayerResource(playerResource));
  }

}
