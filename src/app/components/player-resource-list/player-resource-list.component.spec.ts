import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerResourceListComponent } from './player-resource-list.component';

describe('PlayerResourceListComponent', () => {
  let component: PlayerResourceListComponent;
  let fixture: ComponentFixture<PlayerResourceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerResourceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerResourceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
