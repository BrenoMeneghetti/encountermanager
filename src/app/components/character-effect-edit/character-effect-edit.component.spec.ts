import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterEffectEditComponent } from './character-effect-edit.component';

describe('CharacterEffectsEditComponent', () => {
  let component: CharacterEffectEditComponent;
  let fixture: ComponentFixture<CharacterEffectEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterEffectEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterEffectEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
