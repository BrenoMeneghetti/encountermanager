import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { CharacterEffect } from '../../models/character-effect';
import { TurnBindType } from '../../enumerators/turn-bind-type.enum';
import { TurnCountType } from '../../enumerators/turn-count-type.enum';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SaveType } from '../../enumerators/save-type.enum';

@Component({
  selector: 'app-character-effect-edit',
  templateUrl: './character-effect-edit.component.html',
  styleUrls: ['./character-effect-edit.component.scss']
})
export class CharacterEffectEditComponent implements OnInit {
  private _effect: CharacterEffect;
  @Input() set effect(effect: CharacterEffect) {
    this._effect = effect;
    if (effect) {
      this.effectForm.controls['name'].setValue(effect.name);
      this.effectForm.controls['turnsDuration'].setValue(effect.turnsDuration);
      this.effectForm.controls['turnCountType'].setValue(effect.turnCountType);
      this.effectForm.controls['turnBindType'].setValue(effect.turnBindType);
      this.effectForm.controls['concentration'].setValue(effect.concentration);
      this.effectForm.controls['savePerTurn'].setValue(effect.savePerTurn);
      if (effect.turnsDuration === -1) {
        this.longDuration = true;
      }
    }
  }
  get effect() {
    return this._effect;
  }

  @Output() save = new EventEmitter<CharacterEffect>();
  @Output() cancel = new EventEmitter();
  longDuration: boolean;
  effectForm: FormGroup;
  turnBindOptions = TurnBindType;
  turnBindKeys: any;
  turnCountOptions = TurnCountType;
  turnCountKeys: any;
  saveOptions = SaveType;
  saveKeys: any;

  constructor(private fb: FormBuilder) {
    this.turnBindKeys = Object.keys(this.turnBindOptions);
    this.turnCountKeys = Object.keys(this.turnCountOptions);
    this.saveKeys = Object.keys(this.saveOptions);

    this.effectForm = this.fb.group({
      name: [''],
      turnsDuration: [''],
      turnCountType: [''],
      turnBindType: [''],
      concentration: [''],
      savePerTurn: ['']
    });
  }

  ngOnInit() {
  }

  saveEffect(newEffect: CharacterEffect) {
    const editedEffect = Object.assign({}, this.effect, newEffect);
    this.save.emit(editedEffect);
  }

  cancelEdit() {
    this.cancel.emit();
  }
  changeDuration(isLongDuration: boolean) {
    this.longDuration = !isLongDuration;
    this.effectForm.controls['turnsDuration'].setValue(this.longDuration ? -1 : 0);
  }

}
