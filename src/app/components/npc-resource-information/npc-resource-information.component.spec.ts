import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NPCResourceInformationComponent } from './npc-resource-information.component';

describe('NPCResourceInformationComponent', () => {
  let component: NPCResourceInformationComponent;
  let fixture: ComponentFixture<NPCResourceInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NPCResourceInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NPCResourceInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
