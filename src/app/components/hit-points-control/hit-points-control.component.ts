import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hit-points-control',
  templateUrl: './hit-points-control.component.html',
  styleUrls: ['./hit-points-control.component.scss']
})
export class HitPointsControlComponent implements OnInit {
  @Input() currentHitPoints: number;
  @Input() maximumHitPoints: number;
  editing = false;
  timeoutHandler: any;
  savedValue: number;
  constructor() { }

  ngOnInit() { }

  getBackground(currentHitPoints: number, maximumHitPoints: number) {
    const percentage1 = (currentHitPoints / maximumHitPoints * 100) + '%';
    let color = '#FFF';

    if (this.currentHitPoints === this.maximumHitPoints) {
      color = '#3d48e7';
    }
    if (this.currentHitPoints < this.maximumHitPoints) {
      color = '#69c98f';
    }
    if (this.currentHitPoints <= this.maximumHitPoints * 0.7) {
      color = '#e9e941';
    }
    if (this.currentHitPoints <= this.maximumHitPoints * 0.3) {
      color = '#f87474';
    }

    const backgroundConfig = 'linear-gradient(90deg, ' + color + ' ' + percentage1 + ', #FFF ' + '0%' + ')';
    return backgroundConfig;
  }

  showGreenColor() {
    return this.currentHitPoints < this.maximumHitPoints;
  }

  showYellowColor() {
    return this.currentHitPoints <= this.maximumHitPoints * 0.7;
  }
  showRedColor() {
    return this.currentHitPoints <= this.maximumHitPoints * 0.3;
  }
  showBlackColor() {
    return;
  }

  removeHitPoints() {
    if (this.currentHitPoints) {
      this.currentHitPoints--;
    }
  }
  addHitPoints() {
    if (this.currentHitPoints < this.maximumHitPoints) {
      this.currentHitPoints++;
    }
  }

  mousedownRemoveHitPoints() {
    this.timeoutHandler = setInterval(() => {
      this.removeHitPoints();
    }, 100);
  }

  mouseup() {
    if (this.timeoutHandler) {
      clearInterval(this.timeoutHandler);
      this.timeoutHandler = null;
    }
  }

  mousedownAddHitPoints() {
    this.timeoutHandler = setInterval(() => {
      this.addHitPoints();
    }, 100);
  }
}
