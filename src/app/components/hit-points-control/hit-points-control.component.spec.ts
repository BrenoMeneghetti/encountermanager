import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HitPointsControlComponent } from './hit-points-control.component';

describe('HitPointsControlComponent', () => {
  let component: HitPointsControlComponent;
  let fixture: ComponentFixture<HitPointsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HitPointsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HitPointsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
