import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlayerCharacter } from '../../models/player-character';
import { PlayerResource } from '../../models/player-resource';

@Component({
  selector: 'app-player-information',
  templateUrl: './player-information.component.html',
  styleUrls: ['./player-information.component.scss']
})
export class PlayerInformationComponent implements OnInit {
  @Input() player: PlayerCharacter;
  @Input() playerResources: PlayerResource[];
  @Output() edit = new EventEmitter<PlayerCharacter>();

  constructor() {
  }

  ngOnInit() {}
}
