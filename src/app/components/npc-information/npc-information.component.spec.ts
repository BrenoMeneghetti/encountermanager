import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NPCInformationComponent } from './npc-information.component';

describe('NPCInformationComponent', () => {
  let component: NPCInformationComponent;
  let fixture: ComponentFixture<NPCInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NPCInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NPCInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
