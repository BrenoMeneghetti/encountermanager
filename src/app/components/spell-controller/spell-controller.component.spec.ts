import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpellControllerComponent } from './spell-controller.component';

describe('SpellControllerComponent', () => {
  let component: SpellControllerComponent;
  let fixture: ComponentFixture<SpellControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpellControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpellControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
