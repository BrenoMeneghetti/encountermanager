import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterEffectListComponent } from './character-effect-list.component';

describe('CharacterEffectsListComponent', () => {
  let component: CharacterEffectListComponent;
  let fixture: ComponentFixture<CharacterEffectListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterEffectListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterEffectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
