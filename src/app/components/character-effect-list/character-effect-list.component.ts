import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { PlayerCharacter } from '../../models/player-character';
import { CharacterEffect } from '../../models/character-effect';
import { IAppState } from '../../redux/reducers';
import { Store } from '@ngrx/store';
import { AlertifyService } from '../../services/alertify.service';
import { IncrementEffectCounter } from '../../redux/actions/counters.actions';
import { AddCharacterEffect, UpdateCharacterEffect } from '../../redux/actions/character-effects.actions';

@Component({
  selector: 'app-character-effect-list',
  templateUrl: './character-effect-list.component.html',
  styleUrls: ['./character-effect-list.component.scss']
})
export class CharacterEffectListComponent implements OnInit {
  @Input() visible: boolean;
  @Input() players: PlayerCharacter[];
  @Input() characterEffects: CharacterEffect[];
  @Output() hide = new EventEmitter<boolean>();
  selectedEffect: CharacterEffect;
  effectCounter: number;
  constructor(private store: Store<IAppState>, private alertifyService: AlertifyService) {
    this.store
      .select(state => state.counters.effectCounter).subscribe(effectCounter => this.effectCounter = effectCounter);
  }

  ngOnInit() {
  }

  hideEffects() {
    this.hide.emit(true);
  }

  newEffect() {
    this.selectedEffect = new CharacterEffect();
  }

  cancelEdit() {
    this.selectedEffect = null;
  }

  saveEffect(effect: CharacterEffect) {
    const IsNewEffect: boolean = !effect.id;
    if (!effect.id) {
      this.store.dispatch(new IncrementEffectCounter());
      effect.id = this.effectCounter;
    }
    effect.turnsRemaining = effect.turnsDuration;
    if (IsNewEffect) {
      this.store.dispatch(new AddCharacterEffect(effect));
      this.alertifyService.success('Effect ' + effect.name + ' added.');
    } else {
      this.store.dispatch(new UpdateCharacterEffect(effect));
      this.alertifyService.success('Effect ' + effect.name + ' has been modified.');
    }
    this.selectedEffect = null;

  }

  toggleEffect(effect: CharacterEffect, playerId: number) {
    effect.affectedCharactersId = effect.affectedCharactersId || [];
    if (effect.affectedCharactersId.indexOf(playerId) > -1) {
      effect.affectedCharactersId = effect.affectedCharactersId.filter(id => id !== playerId);
    } else {
      effect.affectedCharactersId[effect.affectedCharactersId.length] = playerId;
    }
    this.store.dispatch(new UpdateCharacterEffect(effect));
  }

  IsCharacterAffected(effect: CharacterEffect, id: number): boolean {
    return effect.affectedCharactersId && effect.affectedCharactersId.indexOf(id) > -1;
  }
}
