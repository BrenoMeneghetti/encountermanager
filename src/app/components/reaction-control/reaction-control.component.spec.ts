import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactionControlComponent } from './reaction-control.component';

describe('ReactionControlComponent', () => {
  let component: ReactionControlComponent;
  let fixture: ComponentFixture<ReactionControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactionControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactionControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
