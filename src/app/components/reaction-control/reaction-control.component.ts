import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reaction-control',
  templateUrl: './reaction-control.component.html',
  styleUrls: ['./reaction-control.component.scss']
})
export class ReactionControlComponent implements OnInit {
  reactionReady = true;
  constructor() { }

  ngOnInit() {
  }

  useReaction() {
    console.log('used');
    this.reactionReady = !this.reactionReady;
  }

}
