import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NPCListComponent } from './npc-list.component';

describe('NPCListComponent', () => {
  let component: NPCListComponent;
  let fixture: ComponentFixture<NPCListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NPCListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NPCListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
