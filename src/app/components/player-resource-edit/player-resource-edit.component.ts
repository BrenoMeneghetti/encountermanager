import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Icons } from '../../enumerators/icons.enum';
import { RecoveryType } from '../../enumerators/recovery-type.enum';
import { AlertifyService } from '../../services/alertify.service';
import { PlayerResource } from '../../models/player-resource';
import { IncrementResourceCounter } from '../../redux/actions/counters.actions';
import { AddPlayerResource, UpdatePlayerResource } from '../../redux/actions/player-resources.actions';
import { EndPlayerResourceEdit } from '../../redux/actions/player-resource-edit.actions';

@Component({
  selector: 'app-player-resource-edit',
  templateUrl: './player-resource-edit.component.html',
  styleUrls: ['./player-resource-edit.component.scss']
})
export class PlayerResourceEditComponent implements OnInit {
  private _resource: PlayerResource;
  @Input() set resource(resource: PlayerResource) {
    this._resource = resource;
    if (resource) {
      this.resourceForm.controls['name'].setValue(resource.name);
      this.resourceForm.controls['maximumQuantity'].setValue(resource.maximumQuantity);
      this.resourceForm.controls['recoveryType'].setValue(resource.recoveryType);
      this.resourceForm.controls['icon'].setValue(resource.icon);
    }
  }
  get resource() {
    return this._resource;
  }
  @Input() resourceCounter: number;

  resourceForm: FormGroup;
  recoveryOptions = RecoveryType;
  iconsOptions = Icons;
  keysRecovery: any;
  @Output() save = new EventEmitter<PlayerResource>();
  @Output() cancel = new EventEmitter<boolean>();

  constructor(private fb: FormBuilder) {
    this.resourceForm = this.fb.group({
      name: [''],
      maximumQuantity: [''],
      recoveryType: [''],
      icon: ['']
    });
    this.keysRecovery = Object.keys(this.recoveryOptions);
  }

  ngOnInit() {
  }

  saveResource(resource: PlayerResource) {
    const editedResource = Object.assign({}, this.resource, resource);
    this.save.emit(editedResource);
  }

  cancelEdit() {
    this.cancel.emit();
  }



}
