import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerResourceEditComponent } from './player-resource-edit.component';

describe('PlayerResourceEditComponent', () => {
  let component: PlayerResourceEditComponent;
  let fixture: ComponentFixture<PlayerResourceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerResourceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerResourceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
