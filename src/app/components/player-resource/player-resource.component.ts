import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { PlayerResource } from '../../models/player-resource';
import { Store } from '@ngrx/store';
import { IAppState } from '../../redux/reducers';
import {
  UpdatePlayerResource,
  RemovePlayerResource
} from '../../redux/actions/player-resources.actions';
import { AlertifyService } from '../../services/alertify.service';
import { StartPlayerResourceEdit } from '../../redux/actions/player-resource-edit.actions';

@Component({
  selector: 'app-player-resource',
  templateUrl: './player-resource.component.html',
  styleUrls: ['./player-resource.component.scss']
})
export class PlayerResourceComponent implements OnInit {
  @Input() characterResource: PlayerResource;
  @Output() edit = new EventEmitter<PlayerResource>();
  @Output() use = new EventEmitter<PlayerResource>();
  @Output() recover = new EventEmitter<PlayerResource>();
  @Output() display = new EventEmitter<PlayerResource>();
  @Output() remove = new EventEmitter<PlayerResource>();
  @HostBinding('class') className = 'column';

  recovering = false;
  resourceList: any[];
  constructor() { }

  ngOnInit() {
    this.resourceList = [];
    for (let i = 0; i < this.characterResource.maximumQuantity; i++) {
      const resourceItem = { available: false };
      resourceItem.available = i < this.characterResource.currentQuantity;
      this.resourceList.push(resourceItem);
    }
    this.className = this.selectColumn(this.characterResource.displayType);
  }

  selectColumn(displayType: number) {
    switch (displayType) {
      case 1: return 'column is-1';
      case 2: return 'column is-2';
      case 3: return 'column is-1';
      case 4: return 'column is-2';
      case 5: return 'column is-3';
    }
  }



  editResource() {
    this.edit.emit(this.characterResource);
  }

  useResource() {
    this.use.emit(this.characterResource);
  }

  recoverResource() {
    this.recover.emit(this.characterResource);
  }

  changeDisplayMode() {
    this.display.emit(this.characterResource);
  }
  removeResource() {
    this.remove.emit(this.characterResource);
  }
}
