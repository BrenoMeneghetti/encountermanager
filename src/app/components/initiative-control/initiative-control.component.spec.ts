import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitiativeControlComponent } from './initiative-control.component';

describe('InitiativeControlComponent', () => {
  let component: InitiativeControlComponent;
  let fixture: ComponentFixture<InitiativeControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitiativeControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitiativeControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
