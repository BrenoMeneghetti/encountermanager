import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-initiative-control',
  templateUrl: './initiative-control.component.html',
  styleUrls: ['./initiative-control.component.scss']
})
export class InitiativeControlComponent implements OnInit {
  @Input() initiativeBonus: number;

  constructor() { }

  ngOnInit() {
  }

}
