import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlayerCharacter } from '../../models/player-character';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../redux/reducers';
import { PlayerResource } from '../../models/player-resource';

@Component({
  selector: 'app-player-list',
  template: `
    <ul class="player-list" >
      <li class="title-line columns">
        <div class="column ">
          Player Name
        </div>
        <div class="column is-1 ">
          Initiative Bonus
        </div>

        <div class="column">
          Hit Points
        </div>

        <div class="column concentration-control">
          Concentration
        </div>
        <div class="column is-three-fifths">
          Resources
        </div>
      </li>
      <li *ngFor="let player of players">
        <app-player-information [player]="player" (edit)="edit.emit($event)"></app-player-information>
      </li>
  </ul>
  `,
  styles: [`
  .player-list{
    list-style-type: none;
    padding: 0;
  }

  .player-list li{
    border-top: 1px solid black;
  }
  .player-list .title-line{
    div{
      text-align: center;
    }
  }
  .player-list .title-line.columns{
    margin-top: 0;
  }
  `]
})
export class PlayerListComponent implements OnInit {
  private _playerResources: PlayerResource[];
  @Input() players: PlayerCharacter[];
  @Output() edit = new EventEmitter<PlayerCharacter>();

  constructor() {
  }

  ngOnInit() { }

}
