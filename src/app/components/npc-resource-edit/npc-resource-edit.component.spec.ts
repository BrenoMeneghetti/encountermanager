import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NPCResourceEditComponent } from './npc-resource-edit.component';

describe('NPCResourceEditComponent', () => {
  let component: NPCResourceEditComponent;
  let fixture: ComponentFixture<NPCResourceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NPCResourceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NPCResourceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
