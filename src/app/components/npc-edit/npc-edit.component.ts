import { Component, OnInit, Input } from '@angular/core';
import { NonPlayerCharacter } from '../../models/non-player-character';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-npc-edit',
  templateUrl: './npc-edit.component.html',
  styleUrls: ['./npc-edit.component.scss']
})
export class NPCEditComponent implements OnInit {
  @Input() npc: NonPlayerCharacter;
  npcForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.npcForm = this.fb.group({
      name: [''],
      initiativeBonus: [''],
      challengeRating: [''],
      maximumHitPoints: [''],
      averageHitPoints: ['']
    });
  }

  ngOnInit() {
  }

}
