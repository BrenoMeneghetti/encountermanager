import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NPCEditComponent } from './npc-edit.component';

describe('NPCEditComponent', () => {
  let component: NPCEditComponent;
  let fixture: ComponentFixture<NPCEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NPCEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NPCEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
