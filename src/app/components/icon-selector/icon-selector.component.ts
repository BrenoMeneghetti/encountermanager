import { Component, OnInit, forwardRef, OnChanges, Input } from '@angular/core';
import { Icons } from '../../enumerators/icons.enum';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl, Validators } from '@angular/forms';



@Component({
  selector: 'app-icon-selector',
  templateUrl: './icon-selector.component.html',
  styleUrls: ['./icon-selector.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => IconSelectorComponent), multi: true },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => IconSelectorComponent), multi: true }
  ]
})
export class IconSelectorComponent implements OnInit, OnChanges, ControlValueAccessor {
  iconsOptions = Icons;
  keysIcons: any;

  // tslint:disable-next-line:no-input-rename
  @Input('icon') _iconClass = '';

  propagateChange: any = () => { };
  validateFn: any = () => { };
  constructor() {
    this.keysIcons = Object.keys(this.iconsOptions).filter(Number);
  }

  get iconValue() {
    return this._iconClass;
  }

  set iconValue(val) {
    this._iconClass = val;
    this.propagateChange(val);
  }

  ngOnInit() {
  }

  ngOnChanges(inputs) {
    if (inputs.counterRangeMax || inputs.counterRangeMin) {
      this.validateFn = Validators.required;
      this.propagateChange(this.iconValue);
    }
  }

  selectIcon(icon: string) {
    this.iconValue = icon;
  }

  writeValue(value) {
    if (value) {
      this.iconValue = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() { }

  validate(c: FormControl) {
    return this.validateFn(c);
  }
  setDisabledState?(isDisabled: boolean): void {
    throw new Error('Method not implemented.');
  }

}
