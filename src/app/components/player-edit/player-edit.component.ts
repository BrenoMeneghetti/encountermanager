import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PlayerCharacter } from '../../models/player-character';

@Component({
  selector: 'app-player-edit',
  templateUrl: './player-edit.component.html',
  styleUrls: ['./player-edit.component.scss']
})
export class PlayerEditComponent implements OnInit {
  playerForm: FormGroup;
  private _player: PlayerCharacter;
  @Input() set player(value: PlayerCharacter) {
    this._player = value;
    if (value) {
      this.playerForm.controls['name'].setValue(value.name);
      this.playerForm.controls['initiativeBonus'].setValue(value.initiativeBonus);
      this.playerForm.controls['maximumHitPoints'].setValue(value.maximumHitPoints);
    }
  }
  get player() {
    return this._player;
  }
  @Output()
  save = new EventEmitter<PlayerCharacter>();
  @Output()
  cancel = new EventEmitter();

  constructor(private fb: FormBuilder) {
    this.playerForm = this.fb.group({
      name: [''],
      initiativeBonus: [''],
      maximumHitPoints: ['']
    });
  }

  ngOnInit() { }

  savePlayer(player: PlayerCharacter) {
    const editedPlayer = Object.assign({}, this.player, player);
    this.save.emit(editedPlayer);
  }

  cancelEdit() {
    this.cancel.emit();
  }
}
