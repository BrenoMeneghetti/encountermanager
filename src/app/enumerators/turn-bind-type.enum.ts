export enum TurnBindType {
  'Select Bind Type' = '',
  'At the source\'s turn' = 'S',
  'At the target\'s turn' = 'T'
}
