export enum TurnCountType {
  'Select Count Type' = '',
  'At the start of the turn' = 'S',
  'At the end of the turn' = 'E'
}
