export enum Icons {
'icons8-aed-2',
'icons8-angel',
'icons8-archer',
'icons8-battle',
'icons8-body-armor',
'icons8-cat-footprint',
'icons8-cat-footprint-2',
'icons8-close-button',
'icons8-clover',
'icons8-danger-sign-with-skull',
'icons8-deaf',
'icons8-defense',
'icons8-defense-filled',
'icons8-dice',
'icons8-octahedron',
'icons8-deltohedron',
'icons8-dodecahedron',
'icons8-icosahedron',
'icons8-edit',
'icons8-edit-2',
'icons8-equestrian-statue',
'icons8-fencing',
'icons8-german-shepherd',
'icons8-german-shepherd-2',
'icons8-grim-reaper',
'icons8-head-silhouette-with-cogwheels',
'icons8-head-silhouette-with-cogwheels-2',
'icons8-hiragana-ki',
'icons8-hiragana-ki-2',
'icons8-horse',
'icons8-judo',
'icons8-mad',
'icons8-mad-2',
'icons8-magic-wand',
'icons8-magic-wand-2',
'icons8-mana',
'icons8-mana-filled',
'icons8-meditation',
'icons8-meditation-2',
'icons8-minecraft-axe',
'icons8-minecraft-sword',
'icons8-minus',
'icons8-minus-2',
'icons8-musical-filled',
'icons8-next-track-button',
'icons8-next-track-button-2',
'icons8-night-portrait',
'icons8-ok',
'icons8-parchment',
'icons8-plus-sign',
'icons8-plus-sign-2',
'icons8-praying-symbol',
'icons8-punching',
'icons8-question-mark',
'icons8-rewind-button',
'icons8-rewind-button-2',
'icons8-shield',
'icons8-skull',
'icons8-slot-machine',
'icons8-sword',
'icons8-treatment',
'icons8-triforce',
'icons8-triggering',
'icons8-xbox-cross',
'icons8-yin-yang-outline',
'icons8-aed'
}
