export enum SaveType {
  'No Save Per Turn' = '',
  'Custom Save' = 'custom',
  'Strength' = 'str',
  'Dexterity' = 'dex',
  'Constitution' = 'con',
  'Intelligence' = 'int',
  'Wisdom' = 'wis',
  'Charisma' = 'cha',
}
