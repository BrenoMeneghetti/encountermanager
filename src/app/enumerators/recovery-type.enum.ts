export enum RecoveryType {
  'Select Recovery Type' = '',
  'Without Recovery' = 'W',
  'Short Rest' = 'S',
  'Long Rest' = 'L'
}
