import { Component } from '@angular/core';
import { StateStorageService } from './services/state-storage.service';
import { Store } from '@ngrx/store';
import { IAppState } from './redux/reducers';
import {
  LoadResourceCounter, LoadCharacterCounter
} from './redux/actions/counters.actions';
import { LoadList } from './redux/actions/player.actions';
import { ReplacePlayerResource } from './redux/actions/player-resources.actions';
import { CharacterEffectListComponent } from './components/character-effect-list/character-effect-list.component';
import { LOAD_CHARACTER_EFFECT_LIST, LoadEffectList } from './redux/actions/character-effects.actions';
import { LoadNPCList } from './redux/actions/npc.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(
    private stateStorageService: StateStorageService,
    private store: Store<IAppState>
  ) {
    const stateFromStorage: IAppState = stateStorageService.loadState();
    if (stateFromStorage) {
      store.dispatch(
        new LoadCharacterCounter(stateFromStorage.counters.characterCounter)
      );
      store.dispatch(
        new LoadResourceCounter(stateFromStorage.counters.resourceCounter)
      );
      store.dispatch(new LoadList(stateFromStorage.players));
      store.dispatch(new ReplacePlayerResource(stateFromStorage.resources));
      store.dispatch(new LoadEffectList(stateFromStorage.characterEffects));
      store.dispatch(new LoadNPCList(stateFromStorage.npcs));
    }
    store.select(state => state).subscribe(state => {
      this.stateStorageService.saveState(state);
    });
  }
}
