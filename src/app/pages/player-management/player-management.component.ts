import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../redux/reducers';
import { Observable } from 'rxjs';
import { PlayerCharacter } from '../../models/player-character';
import { PlayerResource } from '../../models/player-resource';
import { EndPlayerResourceEdit } from '../../redux/actions/player-resource-edit.actions';
import { AddPlayerResource, UpdatePlayerResource } from '../../redux/actions/player-resources.actions';
import {
  StartPlayerEdit,
  EndPlayerEdit
} from '../../redux/actions/player-edit.actions';
import { AddPlayer, UpdatePlayer } from '../../redux/actions/player.actions';
import { IncrementResourceCounter, IncrementCharacterCounter } from '../../redux/actions/counters.actions';
import { AlertifyService } from '../../services/alertify.service';
import { CharacterEffect } from '../../models/character-effect';

@Component({
  selector: 'app-player-management',
  templateUrl: './player-management.component.html',
  styleUrls: ['./player-management.component.scss']
})
export class PlayerManagementComponent implements OnInit {
  players$: Observable<PlayerCharacter[]>;
  effects$: Observable<CharacterEffect[]>;
  characterCount: number;
  playerEdit$: Observable<PlayerCharacter>;
  resourceEdit$: Observable<PlayerResource>;
  characterEffects$: Observable<CharacterEffect[]>;
  resourceCounter: number;
  effectsVisible = false;

  constructor(private store: Store<IAppState>, private alertifyService: AlertifyService) {
    this.players$ = store.select(state => state.players);
    this.playerEdit$ = store.select(state => state.playerEdit);
    this.characterEffects$ = store.select(state => state.characterEffects);
    this.effects$ = store.select(state => state.characterEffects);
    this.store
      .select(state => state.counters.characterCounter)
      .subscribe(characterCount => (this.characterCount = characterCount));

    this.store
      .select(state => state.counters.resourceCounter).subscribe(resourceCounter => this.resourceCounter = resourceCounter);

    this.resourceEdit$ = this.store.select(state => state.playerResourceEdit);

  }

  ngOnInit() { }

  editPlayer(player: PlayerCharacter) {
    this.store.dispatch(new StartPlayerEdit(player));
  }

  startAddPlayer() {
    this.store.dispatch(new StartPlayerEdit(new PlayerCharacter()));
  }

  savePlayer(newPlayer: PlayerCharacter) {
    if (newPlayer.id) {
      if (newPlayer.currentHitPoints > newPlayer.maximumHitPoints || !newPlayer.currentHitPoints) {
        newPlayer.currentHitPoints = newPlayer.maximumHitPoints;
      }
      this.store.dispatch(new UpdatePlayer(newPlayer));
      this.alertifyService.success('Player ' + newPlayer.name + ' has been modified.');
    } else {
      newPlayer.id = this.characterCount;
      newPlayer.currentHitPoints =
        newPlayer.currentHitPoints || newPlayer.maximumHitPoints;
      this.store.dispatch(new AddPlayer(newPlayer));
      this.store.dispatch(new IncrementCharacterCounter());
      this.alertifyService.success('Player ' + newPlayer.name + ' added.');
    }
  }

  saveResource(playerResource: PlayerResource) {
    const IsNewResource: boolean = !playerResource.id;
    if (!playerResource.id) {
      this.store.dispatch(new IncrementResourceCounter());
      playerResource.currentQuantity = playerResource.maximumQuantity;
      playerResource.id = this.resourceCounter;
    } else {
      playerResource.currentQuantity = playerResource.maximumQuantity;
    }
    playerResource.displayType = playerResource.displayType ? playerResource.displayType : 1;

    if (IsNewResource) {
      this.store.dispatch(new AddPlayerResource(playerResource));
      this.alertifyService.success('Resource ' + playerResource.name + ' added.');
    } else {
      this.store.dispatch(new UpdatePlayerResource(playerResource));
      this.alertifyService.success('Resource ' + playerResource.name + ' has been modified.');
    }
    this.store.dispatch(new EndPlayerResourceEdit());
  }

  cancelPlayerResourceEdit() {
    this.store.dispatch(new EndPlayerResourceEdit());
  }

  cancelPlayerEdit() {
    this.store.dispatch(new EndPlayerEdit());
  }
}
