import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NpcManagementComponent } from './npc-management.component';

describe('NpcManagementComponent', () => {
  let component: NpcManagementComponent;
  let fixture: ComponentFixture<NpcManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NpcManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NpcManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
